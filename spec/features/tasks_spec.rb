require 'spec_helper'
describe PostsController do 

  
  ##Test post page with creating new comment
  describe "Post Create Comments" do
    it "creates comment" do
      @post = Post.create!(:title => "First Post", :body => "First post's body.")
      visit post_path(@post)
      fill_in "New Comment", :with => "Comment for first post."
      page.should have_content("Comment for first post.")
      
    end
    
    it "creates comment successfully" do
      @post = Post.create!(:title => "First Post", :body => "First post's body.")
      visit post_path(@post)
      fill_in "New Comment", :with => "Comment for first post."
      click_button "Add Comment"
      page.should have_content("Comment was successfully created.")
    end
  end
  
  describe "New Comments Have Correct Order" do

    it ", new comment comes before previous comment" do
      @post = Post.create!(:title => "First Post", :body => "First post's body.")
      visit post_path(@post)
      fill_in "New Comment", :with => "First Comment."
      click_button "Add Comment"
      fill_in "New Comment", :with => "Second Comment."
      click_button "Add Comment"
      
      
      page.should have_selector("#comments div:nth-child(1)", :text => "Second Comment.")
      page.should have_selector("#comments div:nth-child(2)", :text => "First Comment.")
    end
    it ", new comment comes before previous comment" do
      @post = Post.create!(:title => "First Post", :body => "First post's body.")
      visit post_path(@post)
      fill_in "New Comment", :with => "First Comment."
      click_button "Add Comment"
      fill_in "New Comment", :with => "Second Comment."
      click_button "Add Comment"
      
      
      page.should have_selector("#comments div:nth-child(1)", :text => "Second Comment.")
      page.should have_selector("#comments div:nth-child(2)", :text => "First Comment.")
    end
    
    # it ", test ajax partial render" do
    #   @post = Post.create!(:title => "First Post", :body => "First post's body.")
    #   visit post_path(@post)
    #   fill_in "New Comment", :with => "First Comment."
    #   click_button "Add Comment"
    #   #should render_template('/comments/create') 
    # end
  end
  # describe "Posts page renders _comment partial" do
  #   get :show
  #   it "renders partial" do
  #     @post = Post.create!(:title => "First Post", :body => "First post's body.")
  #     get post_path(@post)
  #     response.should render_template(:partial => "_comment")
  #   end
  # end
  
end
